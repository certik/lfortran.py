-- Abstract Semantic Representation (ASR) definition

-- ASDL's builtin types are:
--   * identifier
--   * int (signed integers of infinite precision)
--   * string
-- We extend these by:
--   * object (any Python object)
--   * constant (.true. / .false.)
--   * symbol_table (scoped Symbol Table implementation)
--   * node (any ASR node)
--
-- Note: `symbol_table` contains `Module`, `Subroutine`, `Function`, `Variable`.

module ASR {

unit
    = TranslationUnit(symbol_table global_scope, node* items)

prog
    = Program(identifier name, stmt* body, symbol_table symtab)

mod
    = Module(identifier name, object symtab)

sub
    = Subroutine(identifier name, expr* args, stmt* body, tbind? bind,
            symbol_table symtab)

fn
    = Function(identifier name, expr* args, stmt* body, tbind? bind,
            expr return_var, string? module, symbol_table symtab)
var
-- intent:
-- =0 ... local variable,
-- >0 ... dummy argument: 1=in, 2=out, 3=inout
    = Variable(identifier name, int intent, ttype type)

stmt
    = Assignment(expr target, expr value)
    | SubroutineCall(sub name, expr* args)
    | BuiltinCall(identifier name, expr* args)
    | If(expr test, stmt* body, stmt* orelse)
    | Where(expr test, stmt* body, stmt* orelse)
    | Stop(expr? code)
    | ErrorStop(expr? code)
    | DoLoop(do_loop_head? head, stmt* body)
    | Select(expr test, case_stmt* body, case_default? default)
    | Cycle()
    | Exit()
    | WhileLoop(expr test, stmt* body)
    | Print(string? fmt, expr* values)

expr
    = BoolOp(expr left, boolop op, expr right, ttype type)
    | BinOp(expr left, operator op, expr right, ttype type)
    | UnaryOp(unaryop op, expr operand, ttype type)
    | Compare(expr left, cmpop op, expr right, ttype type)
    | FuncCall(fn func, expr* args, keyword* keywords, ttype type)
    | ArrayRef(identifier name, array_index* args, ttype type)
    | ArrayInitializer(expr* args, ttype type)
    | Num(object n, ttype type)
    | Str(string s, ttype type)
    | VariableOld(identifier name, string? intent, int? dummy, ttype type)
    | Var(symbol_table symtab, var v)
    | Constant(constant value, ttype type)

ttype
    = Integer(int kind, dimension* dims)
    | Real(int kind, dimension* dims)
    | Complex(int kind, dimension* dims)
    | Character(int kind, dimension* dims)
    | Logical(int kind, dimension* dims)
    | Derived(string name, dimension* dims, string? module)

boolop = And | Or

-- TODO: rename to binop:
operator = Add | Sub | Mul | Div | Pow

unaryop = Invert | Not | UAdd | USub

cmpop = Eq | NotEq | Lt | LtE | Gt | GtE

dimension = (expr? start, expr? end)

attribute = Attribute(identifier name, attribute_arg *args)

attribute_arg = (identifier arg)

arg = (identifier arg)

keyword = (identifier? arg, expr value)

tbind = Bind(string lang, string name)

array_index = ArrayIndex(expr? left, expr? right, expr? step)

do_loop_head = (expr v, expr start, expr end, expr? increment)

case_stmt = (expr test, stmt* body)

case_default = (stmt* body)

}
