# LFortran Python wrappers

Python wrappers to the C++ code / library
[LFortran](https://gitlab.com/lfortran/lfortran),
a modern open-source (BSD licensed) interactive Fortran compiler built on top of
LLVM.
